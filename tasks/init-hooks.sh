#!/bin/bash

# Initialize or update hooks in current project
#
# Environment variables:
#   DEV_TOOLS: URL to the dev tools repository

: "${DEV_TOOLS:=https://gitlab.com/sat-mtl/valorisation/dev-tools/-/raw/main}"

CONFIG_HOOKS="$DEV_TOOLS/hooks"

#######################################
# Downloads a pre-commit config
# Arguments:
#   Type of the pre-config hook
#######################################
downloadPreCommitConfig () {
  local configType="${1}"
  local targetFile="${2}"

  curl -L "$CONFIG_HOOKS/${configType}.yaml" >> "${targetFile}"
}

#######################################
# Merge pre-commit config parts
#######################################
mergeAllPreCommitConfigs () {
  local options="$1"
  local targetFile="${2}"

  rm "${targetFile}"
  curl -L "$CONFIG_HOOKS/prelude.yaml" > "${targetFile}"

  for option in $options; do
    case "$option" in
      common) downloadPreCommitConfig "common" "${targetFile}";;
      py) downloadPreCommitConfig "py" "${targetFile}";;
      js) downloadPreCommitConfig "js" "${targetFile}";;
      cpp) downloadPreCommitConfig "cpp" "${targetFile}";;
      php) downloadPreCommitConfig "php" "${targetFile}";;
    esac
  done
}

if [ "$#" -eq "1" ]; then
  mergeAllPreCommitConfigs "$1" ".pre-commit-config.yaml"
fi
